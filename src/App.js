import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import Notfound from "../src/app/component/notfound/Notfound";
import { adminRouter, publicRouter } from "./app/route/route";
import { createContext, useEffect, useRef, useState } from "react";
import Loading from "./app/component/loading/Loading";
import { jwtDecode } from "jwt-decode";
export const UserContext = createContext();
function App() {
  const [loading, setLoading] = useState(false);

  const cacheRef = useRef({});

  const [role, setRole] = useState("");
  const [reload, setReload] = useState(false);

  const checkAdmin = (token) => {
    try {
      const decoded = jwtDecode(token);
      setRole(decoded?.role);
    } catch (error) {
      return true; // Error in decoding or expired
    }
  };

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token) {
      checkAdmin(token);
    }
  }, [reload]);

  return (
    <UserContext.Provider
      value={{
        setLoading,
        cache: cacheRef.current,
        setReload,
      }}
    >
      <Router>
        <div>
          <Routes>
            {publicRouter.map((item, index) => {
              const Page = item.element;
              return item.layout ? (
                <Route
                  key={index + "routerpriva"}
                  path={item.path}
                  element={
                    <item.layout type={item?.type}>
                      <Page />
                    </item.layout>
                  }
                />
              ) : (
                <Route
                  key={item?.path + index}
                  path={item?.path}
                  element={<Page />}
                />
              );
            })}
            {role === "ADMIN" &&
              adminRouter.map((item, index) => {
                const Page = item.element;
                return item.layout ? (
                  <Route
                    key={index + "routerpriva"}
                    path={item.path}
                    element={
                      <item.layout type={item?.type}>
                        <Page />
                      </item.layout>
                    }
                  />
                ) : (
                  <Route
                    key={item?.path + index}
                    path={item?.path}
                    element={<Page />}
                  />
                );
              })}
            <Route path="*" element={<Notfound />} />
          </Routes>
          <ToastContainer autoClose={1500} style={{ fontSize: "15px" }} />
          {loading && <Loading />}
        </div>
      </Router>
    </UserContext.Provider>
  );
}

export default App;
