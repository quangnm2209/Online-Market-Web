import React, {
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import { useDropzone } from "react-dropzone";
import "./customStyle.scss";
import { toast } from "react-toastify";
import ImageService from "../service/ImageService";
import ThemeService from "../service/ThemeService";
import { UserContext } from "../../App";
import { useLocation } from "react-router-dom";
import Pagination from "../component/pagination/Pagination";
const Themes = () => {
  const [create, setCreate] = useState(false);
  const searchRef = useRef(null);
  const [edit, setEdit] = useState(null);

  const [reload, setReload] = useState(false);
  const [themes, setThemes] = useState([]);
  const { setLoading } = useContext(UserContext);
  const [totalPage, setTotalPage] = useState(1);

  const { search } = useLocation();
  const getQueryParam = (name) => {
    const searchParams = new URLSearchParams(search);
    return searchParams.get(name) || (name == "page" ? "1" : "");
  };

  const hanldeGetAllTheme = async (param) => {
    try {
      setLoading(true);
      const data = await ThemeService.getAll(param);
      setThemes(data?.data?.datas || []);
      setTotalPage(data?.data?.totalPage || 1);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      toast.error(err?.response?.data?.message);
    }
  };
  useEffect(() => {
    handleSearch();
  }, [reload, search]);

  const handleDeleteTheme = async (title, id) => {
    try {
      const checked = window.confirm(`Bạn có thực sự muốn xóa ${title} không?`);
      if (checked) {
        const data = await ThemeService.delete(id);
        toast.success(data?.data?.message);
        setReload((pre) => !pre);
      }
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };
  const handleSearch = () => {
    const filteredSearch = {
      search: searchRef.current.value,
      page: getQueryParam("page"),
      size: 10,
    };
    const excludesFields = ["search"];
    excludesFields?.forEach((item) => {
      if (!filteredSearch[item]) {
        delete filteredSearch[item];
      }
    });
    const searchURL = new URLSearchParams(filteredSearch);
    hanldeGetAllTheme(searchURL.toString());
  };
  return (
    <div>
      <div
        style={{
          display: "flex",
          marginTop: "20px",
          justifyContent: "flex-end",
        }}
      >
        <button
          onClick={() => {
            setCreate(true);
            setEdit(null);
          }}
          className="btn btn-primary"
        >
          Thêm mới theme
        </button>
      </div>
      <div className="search_form">
        <div className="search_form_wrap">
          <div className="search_form_item">
            <input
              ref={searchRef}
              className="search_form_input"
              placeholder="Tìm kiếm..."
            />
          </div>
          <div>
            <div>
              <button
                onClick={handleSearch}
                className="btn btn-primary"
                style={{
                  borderRadius: "40px",
                  height: "34px",
                  fontSize: "13px",
                }}
              >
                Tìm kiếm
              </button>
            </div>
          </div>
        </div>
      </div>
      {(!themes || themes?.length == 0) && (
        <div style={{ textAlign: "center", width: "100%" }}>
          <p style={{ fontSize: "16px", color: "red" }}>
            (Không tìm thấy dữ liệu)
          </p>
        </div>
      )}
      <section style={{ marginTop: "15px" }} className="ftco-section">
        <div className="row">
          <div className="col-md-12">
            <div className="table-wrap">
              <table className="table table-responsive-xl">
                <thead>
                  <tr>
                    <th style={{ width: "2%" }}>ID</th>
                    <th style={{ width: "10%" }}>Tên theme</th>
                    <th style={{ width: "10%" }}>Ảnh thumbnail</th>
                    <th style={{ width: "10%" }}>Màu background (tổng)</th>
                    <th style={{ width: "10%" }}>Màu chữ (tổng)</th>
                    <th style={{ width: "10%" }}>Background (ảnh)</th>
                    <th style={{ width: "10%" }}>Màu Background (ảnh)</th>
                    <th style={{ width: "10%" }}>Màu chữ (ảnh)</th>
                    <th style={{ width: "5%" }}>Opacity</th>
                    <th style={{ width: "15%" }}>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  {themes?.map((item) => (
                    <tr
                      key={item?.themeID + "Theme"}
                      className="alert"
                      role="alert"
                    >
                      <td className="border-bottom-0-custom">
                        {item?.themeID}
                      </td>
                      <td className="border-bottom-0-custom">
                        {item?.themeName}
                      </td>
                      <td className="border-bottom-0-custom">
                        {item?.thumbnailImage ? (
                          <img
                            style={{ width: "30px", height: "30px" }}
                            src={item?.thumbnailImage}
                          />
                        ) : (
                          <div>(Không có)</div>
                        )}
                      </td>
                      <td className="border-bottom-0-custom">
                        {item?.bgColor ? (
                          <div
                            style={{
                              backgroundColor: item?.bgColor,
                              width: "30px",
                              height: "30px",
                              border: "1px solid rgba(0,0,0,0.1)",
                            }}
                          ></div>
                        ) : (
                          <div>(Không có)</div>
                        )}
                      </td>
                      <td className="border-bottom-0-custom">
                        {item?.textColor ? (
                          <div
                            style={{
                              backgroundColor: item?.textColor,
                              width: "30px",
                              height: "30px",
                              border: "1px solid rgba(0,0,0,0.1)",
                            }}
                          ></div>
                        ) : (
                          <div>(Không có)</div>
                        )}
                      </td>
                      <td className="border-bottom-0-custom">
                        {item?.cardBgImage ? (
                          <img
                            style={{ width: "30px", height: "30px" }}
                            src={item?.cardBgImage}
                          />
                        ) : (
                          <div>(Không có)</div>
                        )}
                      </td>
                      <td className="border-bottom-0-custom">
                        {item?.cardBgColor ? (
                          <div
                            style={{
                              backgroundColor: item?.cardBgColor,
                              width: "30px",
                              height: "30px",
                              border: "1px solid rgba(0,0,0,0.1)",
                            }}
                          ></div>
                        ) : (
                          <div>(Không có)</div>
                        )}
                      </td>
                      <td className="border-bottom-0-custom">
                        {item?.cardTextColor ? (
                          <div
                            style={{
                              backgroundColor: item?.cardTextColor,
                              width: "30px",
                              height: "30px",
                              border: "1px solid rgba(0,0,0,0.1)",
                            }}
                          ></div>
                        ) : (
                          <div>(Không có)</div>
                        )}
                      </td>
                      <td className="border-bottom-0-custom">
                        {item?.bgOpacity}
                      </td>
                      <td className="border-bottom-0-custom">
                        <button
                          onClick={() => {
                            setEdit(item);
                            setCreate(true);
                          }}
                          style={{
                            marginLeft: "5px",
                            height: "30px",
                            fontSize: "12px",
                          }}
                          type="button"
                          className="btn btn-primary"
                        >
                          Sửa
                        </button>
                        <button
                          onClick={() => {
                            handleDeleteTheme(item?.themeName, item?.themeID);
                          }}
                          style={{
                            marginLeft: "5px",
                            height: "30px",
                            fontSize: "12px",
                          }}
                          type="button"
                          className="btn btn-secondary"
                        >
                          Xóa
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>
      <Pagination count={totalPage} />
      {/* {accounts?.accounts && <Pagination count={accounts?.total} />} */}
      {create && (
        <CreateTheme setActive={setCreate} setReload={setReload} edit={edit} />
      )}
    </div>
  );
};
const CreateTheme = ({ setActive, setReload, edit }) => {
  const { setLoading } = useContext(UserContext);

  const [thumbnailSrc, setThumbnailSrc] = useState(null);
  const [thumbnail, setthumbnail] = useState(null);

  const [cardBackground, setcardBackground] = useState(null);
  const [cardBackgroundSrc, setCardBackgroundSrc] = useState(null);

  const [cardBgColor, setcardBgColor] = useState("");
  const [cardTextColor, setcardTextColor] = useState("");
  const [bgOpacity, setbgOpacity] = useState(100);
  const [bgColor, setbgColor] = useState("");
  const [textColor, settextColor] = useState("");

  const [cardBackgroundSrcSelection, setcardBackgroundSrcSelection] =
    useState(false);
  const [thumbnailSrcSelection, setthumbnailSrcSelection] = useState(false);
  const [cardBgColorSelection, setcardBgColorSelection] = useState(false);
  const [cardTextColorSelection, setcardTextColorSelection] = useState(false);
  const [bgColorSelection, setbgColorSelection] = useState(false);
  const [textColorSelection, settextColorSelection] = useState(false);

  const themeNameRef = useRef();
  const thumnailRef = useRef();
  const bgCardRef = useRef();
  const bgColorRef = useRef();
  const cardBgColorRef = useRef();
  const textColorRef = useRef();
  const cardTextColorRef = useRef();

  const onDropThumbnail = useCallback((acceptedFiles) => {
    const file = acceptedFiles[0];
    if (file) {
      setthumbnail(file);
      const img = new Image();
      const reader = new FileReader();
      reader.onload = (e) => {
        img.src = e.target.result;
      };
      img.onload = () => {
        setThumbnailSrc(img.src);
      };
      reader.readAsDataURL(file);
    }
  }, []);

  const onDropBackground = useCallback((acceptedFiles) => {
    const file = acceptedFiles[0];
    if (file) {
      setcardBackground(file);
      const img = new Image();
      const reader = new FileReader();
      reader.onload = (e) => {
        img.src = e.target.result;
      };
      img.onload = () => {
        setCardBackgroundSrc(img.src);
      };
      reader.readAsDataURL(file);
    }
  }, []);

  const {
    getRootProps: getRootPropsThumbnail,
    getInputProps: getInputPropsThumbnail,
  } = useDropzone({
    onDrop: onDropThumbnail,
    accept: "image/*",
    maxFiles: 1,
  });

  const {
    getRootProps: getRootPropsBackground,
    getInputProps: getInputPropsBackground,
  } = useDropzone({
    onDrop: onDropBackground,
    accept: "image/*",
    maxFiles: 1,
  });

  const [error, setError] = useState(false);

  const handleCreateNewTheme = async () => {
    try {
      let theme = {
        themeName: themeNameRef.current.value,
        bgColor: bgColorRef.current.checked ? bgColor : "white",
        textColor: textColorRef.current.checked ? textColor : "black",
        cardBgColor: cardBgColorRef.current.checked
          ? cardBgColor
          : "rgba(234, 152, 91, 0.15)",
        cardTextColor: cardTextColorRef.current.checked
          ? cardTextColor
          : "black",
        cardBgImage: bgCardRef.current.checked ? cardBackgroundSrc : "",
        thumbnailImage: thumnailRef.current.checked ? thumbnailSrc : "",
        bgOpacity: bgOpacity / 100,
      };
      if (!theme?.themeName) {
        setError(true);
        return;
      }
      setLoading(true);
      if (cardBackground && bgCardRef.current.checked) {
        theme.cardBgImage = await ImageService.uploadImagesToServer3S(
          cardBackground,
          process.env.REACT_APP_THEME_FILE
        );
      }
      if (thumbnail && thumnailRef.current.checked) {
        theme.thumbnailImage = await ImageService.uploadImagesToServer3S(
          thumbnail,
          process.env.REACT_APP_THEME_FILE
        );
      }
      if (!thumnailRef.current.checked) {
        let thumbnailDefaultURL =
          "https://res.cloudinary.com/sttruyen/image/upload/v1718775583/Sttruyenxyz/jsgvtgo9armupbuyznvj.jpg";
        theme.thumbnailImage = await ImageService.updateImageURLToServer3S(
          thumbnailDefaultURL,
          process.env.REACT_APP_THEME_FILE
        );
      }
      if (!edit) {
        const data = await ThemeService.create(theme);
        toast.success(data?.data?.message);
      } else {
        const data = await ThemeService.update({
          ...theme,
          themeID: edit?.themeID,
        });
        toast.success(data?.data?.message);
      }
      setLoading(false);
      setReload((pre) => !pre);
      setError(false);
      setActive(false);
    } catch (err) {
      setLoading(false);
      toast.error(err?.response?.data?.message);
    }
  };

  useEffect(() => {
    if (edit) {
      themeNameRef.current.value = edit?.themeName;
      setThumbnailSrc(edit?.thumbnailImage || "");
      setCardBackgroundSrc(edit?.cardBgImage || "");
      thumnailRef.current.checked =
        edit?.thumbnailImage &&
        edit?.thumbnailImage !=
          "https://res.cloudinary.com/sttruyen/image/upload/v1718775583/Sttruyenxyz/jsgvtgo9armupbuyznvj.jpg"
          ? true
          : false;
      bgCardRef.current.checked = edit?.cardBgImage;
      bgColorRef.current.checked =
        edit?.bgColor && edit?.bgColor != "white" ? true : false;
      cardBgColorRef.current.checked =
        edit?.cardBgColor && edit?.cardBgColor != "rgba(234, 152, 91, 0.15)"
          ? true
          : false;
      textColorRef.current.checked =
        edit?.textColor && edit?.textColor != "black" ? true : false;
      cardTextColorRef.current.checked =
        edit?.cardTextColor && edit?.cardTextColor != "black" ? true : false;

      setbgColor(edit?.bgColor);
      settextColor(edit?.textColor);
      setcardBgColor(edit?.cardBgColor);
      setcardTextColor(edit?.cardTextColor);

      setthumbnailSrcSelection(
        edit?.thumbnailImage &&
          edit?.thumbnailImage !=
            "https://res.cloudinary.com/sttruyen/image/upload/v1718775583/Sttruyenxyz/jsgvtgo9armupbuyznvj.jpg"
          ? true
          : false
      );
      setcardBgColorSelection(
        edit?.cardBgColor && edit?.cardBgColor != "rgba(234, 152, 91, 0.15)"
          ? true
          : false
      );
      setbgColorSelection(
        edit?.bgColor && edit?.bgColor != "white" ? true : false
      );
      settextColorSelection(
        edit?.textColor && edit?.textColor != "black" ? true : false
      );
      setcardBackgroundSrcSelection(edit?.cardBgImage ? true : false);
      setcardTextColorSelection(
        edit?.cardTextColor && edit?.cardTextColor != "black" ? true : false
      );
    }
  }, [edit]);

  return (
    <div className="create_container">
      <div className="create_theme_wrap">
        <div
          style={{
            backgroundColor: bgColorSelection ? bgColor : "white",
            color: textColorSelection ? textColor : "black",
          }}
          className="theme_preview"
        >
          <div className="theme_title">
            <div>
              <i className="fa-regular fa-circle-left"></i>
            </div>
            <div className="them_title_txt">Cửa hàng của Online Market</div>
          </div>
          <div className="theme_image">
            <img
              src={
                thumbnailSrcSelection
                  ? thumbnailSrc
                  : "https://res.cloudinary.com/sttruyen/image/upload/v1718775583/Sttruyenxyz/jsgvtgo9armupbuyznvj.jpg"
              }
            />
          </div>
          <div className="theme_infor">
            <div className="theme_avatar">
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719146312/bfgpcwec48tlpyddyvhf.png" />
            </div>
            <div className="theme_infor_title">
              <div style={{ fontSize: "14px" }}>
                <b>Online Market</b>
              </div>
              <div>Địa chỉ: Online Market</div>
            </div>
            <div>
              <i
                style={{ fontSize: "30px", cursor: "pointer" }}
                className="fa-brands fa-gratipay"
              ></i>
            </div>
          </div>
          <div className="theme_br"></div>
          <div className="theme_card_wrap">
            <WithoutImageCard
              cardTextColor={cardTextColorSelection ? cardTextColor : ""}
              cardBgColor={cardBgColorSelection ? cardBgColor : ""}
              opacity={bgOpacity / 100}
              bgImage={cardBackgroundSrcSelection ? cardBackgroundSrc : ""}
            />
            <WithoutImageCard
              cardTextColor={cardTextColorSelection ? cardTextColor : ""}
              cardBgColor={cardBgColorSelection ? cardBgColor : ""}
              opacity={bgOpacity / 100}
              bgImage={cardBackgroundSrcSelection ? cardBackgroundSrc : ""}
            />
            <WithImageCard
              cardTextColor={cardTextColorSelection ? cardTextColor : ""}
              cardBgColor={cardBgColorSelection ? cardBgColor : ""}
              opacity={bgOpacity / 100}
              bgImage={cardBackgroundSrcSelection ? cardBackgroundSrc : ""}
            />
            <WithImageCard
              cardTextColor={cardTextColorSelection ? cardTextColor : ""}
              cardBgColor={cardBgColorSelection ? cardBgColor : ""}
              opacity={bgOpacity / 100}
              bgImage={cardBackgroundSrcSelection ? cardBackgroundSrc : ""}
            />
          </div>
        </div>
        <div className="theme_edit">
          <div
            onClick={() => {
              setActive(false);
            }}
            className="create_close_wrap"
          >
            <i className="fa-solid fa-x"></i>
          </div>
          <div style={{ width: "100%" }}>
            <div style={{ display: "flex" }}>
              <div style={{ flex: 1, alignItems: "center" }}>
                <div
                  className="theme_label"
                  style={{
                    justifyContent: "center",
                    marginTop: "10px",
                    marginBottom: "-10px",
                  }}
                >
                  Ảnh thumbnail
                  <input
                    style={{ marginLeft: "5px", height: "20px", width: "15px" }}
                    type="checkbox"
                    defaultValue={thumbnailSrcSelection}
                    onChange={(e) => {
                      setthumbnailSrcSelection(e.target.checked);
                    }}
                    ref={thumnailRef}
                  />
                </div>
                <div
                  {...getRootPropsThumbnail()}
                  className="dropzone_container"
                >
                  <input {...getInputPropsThumbnail()} />
                  {thumbnailSrc ? (
                    <img
                      src={thumbnailSrc}
                      alt="Uploaded"
                      className="dropzone_image"
                    />
                  ) : (
                    <p>Ảnh thumbnail</p>
                  )}
                </div>
              </div>
              <div style={{ flex: 1, alignItems: "center" }}>
                <div
                  className="theme_label"
                  style={{
                    justifyContent: "center",
                    marginTop: "10px",
                    marginBottom: "-10px",
                  }}
                >
                  Ảnh background của thẻ
                  <input
                    style={{ marginLeft: "5px", height: "20px", width: "15px" }}
                    type="checkbox"
                    defaultValue={cardBackgroundSrcSelection}
                    onChange={(e) => {
                      setcardBackgroundSrcSelection(e.target.checked);
                    }}
                    ref={bgCardRef}
                  />
                </div>
                <div
                  {...getRootPropsBackground()}
                  className="dropzone_container"
                >
                  <input {...getInputPropsBackground()} />
                  {cardBackgroundSrc ? (
                    <img
                      src={cardBackgroundSrc}
                      alt="Uploaded"
                      className="dropzone_image"
                    />
                  ) : (
                    <p>Ảnh background của thẻ</p>
                  )}
                </div>
              </div>
            </div>
            <div className="theme_input_wrap">
              <div className="theme_input">
                <div className="theme_label">
                  Màu background
                  <input
                    style={{ marginLeft: "5px", height: "20px", width: "15px" }}
                    type="checkbox"
                    defaultValue={bgColorSelection}
                    onChange={(e) => {
                      setbgColorSelection(e.target.checked);
                    }}
                    ref={bgColorRef}
                  />
                  :
                </div>
                <input
                  type="color"
                  placeholder="Thể loại mới *"
                  className="create_input"
                  style={{ width: "50px" }}
                  onChange={(e) => {
                    setbgColor(e.target.value);
                  }}
                  defaultValue={edit?.bgColor || ""}
                />
              </div>
              <div className="theme_input">
                <div className="theme_label">
                  Màu background thẻ
                  <input
                    style={{ marginLeft: "5px", height: "20px", width: "15px" }}
                    type="checkbox"
                    defaultValue={cardBgColorSelection}
                    onChange={(e) => {
                      setcardBgColorSelection(e.target.checked);
                    }}
                    ref={cardBgColorRef}
                  />
                  :
                </div>
                <input
                  type="color"
                  placeholder="Thể loại mới *"
                  className="create_input"
                  style={{ width: "50px" }}
                  defaultValue={edit?.cardBgColor || ""}
                  onChange={(e) => {
                    setcardBgColor(e.target.value);
                  }}
                />
              </div>
              <div className="theme_input">
                <div className="theme_label">
                  Màu chữ tổng
                  <input
                    style={{ marginLeft: "5px", height: "20px", width: "15px" }}
                    type="checkbox"
                    defaultValue={textColorSelection}
                    onChange={(e) => {
                      settextColorSelection(e.target.checked);
                    }}
                    ref={textColorRef}
                  />
                  :
                </div>
                <input
                  type="color"
                  className="create_input"
                  style={{ width: "50px" }}
                  defaultValue={edit?.textColor || ""}
                  onChange={(e) => {
                    settextColor(e.target.value);
                  }}
                />
              </div>
              <div className="theme_input">
                <div className="theme_label">
                  Màu chữ của thẻ
                  <input
                    style={{ marginLeft: "5px", height: "20px", width: "15px" }}
                    type="checkbox"
                    defaultValue={cardTextColorSelection}
                    onChange={(e) => {
                      setcardTextColorSelection(e.target.checked);
                    }}
                    ref={cardTextColorRef}
                  />
                  :
                </div>
                <input
                  type="color"
                  placeholder="Thể loại mới *"
                  className="create_input"
                  style={{ width: "50px" }}
                  defaultValue={edit?.cardTextColor || ""}
                  onChange={(e) => {
                    setcardTextColor(e.target.value);
                  }}
                />
              </div>
              <div className="theme_input">
                <div className="theme_label">Độ mờ (100%):</div>
                <input
                  type="range"
                  placeholder="Thể loại mới *"
                  style={{ width: "70px" }}
                  min="0"
                  max="100"
                  defaultValue={bgOpacity}
                  onChange={(e) => {
                    setbgOpacity(e.target?.value);
                  }}
                />
              </div>
            </div>
            <div style={{ textAlign: "center", padding: "0 20px" }}>
              <i>
                (Tích vào ô để chọn có sử dụng thuộc tính này nếu không thuộc
                tính đó sẽ mặc định không có giá trị)
              </i>
            </div>
            <div style={{ marginTop: "20px" }}>
              <div className="create_input_container">
                <input
                  placeholder="Tên theme *"
                  className="create_input"
                  name="themeName"
                  ref={themeNameRef}
                />
              </div>
              {error && (
                <div
                  style={{
                    color: "red",
                    textAlign: "center",
                    marginTop: "10px",
                  }}
                >
                  Tên theme không được bỏ trống!
                </div>
              )}
            </div>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              marginBottom: "10px",
            }}
          >
            <button onClick={handleCreateNewTheme} className="btn btn-primary">
              {edit ? "Cập nhật" : "Tạo mới"}
            </button>
            <button
              onClick={() => {
                setActive(false);
              }}
              style={{ marginLeft: "10px" }}
              className="btn btn-secondary"
            >
              Hủy
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
const WithoutImageCard = ({
  cardTextColor = "black",
  cardBgColor = "",
  opacity = 1,
  bgImage = "",
}) => {
  return (
    <div
      className="theme_card"
      style={{
        color: cardTextColor,
        backgroundColor: cardBgColor,
        backgroundImage: `url(${bgImage})`,
        backgroundPosition: "center",
        backgroundSize: "cover",
        opacity: opacity,
      }}
    >
      <div></div>
      <div className="theme_card_infor">
        <div style={{ fontSize: "13px" }}>
          <b>Táo</b>
        </div>
        <div style={{ fontSize: "12px" }}>
          <b>Người bán: Bà Tám</b>
        </div>
        <div style={{ marginTop: "3px", fontSize: "11px" }}>
          20.000VND / 1kg
        </div>
      </div>
      <div className="theme_card_btn_wrap">
        <button
          style={{
            color: cardTextColor,
          }}
        >
          Mua hàng
        </button>
        <button
          style={{
            color: cardTextColor,
          }}
        >
          Cửa hàng
        </button>
      </div>
    </div>
  );
};
const WithImageCard = ({
  cardTextColor = "black",
  cardBgColor = "",
  opacity = 1,
  bgImage = "",
}) => {
  return (
    <div
      className="theme_card"
      style={{
        padding: 0,
        overflow: "hidden",
        height: "170px",
        color: cardTextColor,
        backgroundColor: cardBgColor,
        backgroundImage: `url(${bgImage})`,
        opacity: opacity,
        backgroundPosition: "center",
        backgroundSize: "cover",
      }}
    >
      <div>
        <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719144235/Sttruyenxyz/ppccgwlnfhkb7uwebiqb.jpg" />
      </div>
      <div style={{ padding: "5px 5px", marginTop: "-5px" }}>
        <div className="theme_card_infor">
          <div style={{ fontSize: "13px" }}>
            <b>Táo</b>
          </div>
          <div style={{ fontSize: "12px" }}>
            <b>Người bán: Bà Tám</b>
          </div>
          <div style={{ marginTop: "3px", fontSize: "11px" }}>
            20.000VND / 1kg
          </div>
        </div>
        <div className="theme_card_btn_wrap">
          <button>Mua hàng</button>
          <button>Cửa hàng</button>
        </div>
      </div>
    </div>
  );
};

export default Themes;
