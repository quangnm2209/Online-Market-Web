import React, { useEffect, useRef, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import { toast } from "react-toastify";
import ProductService from "../service/ProductService";
import Pagination from "../component/pagination/Pagination";
import Select from "react-select";
import ProductCard from "../component/card/ProductCard";

const options = [
  { value: "DATE_DECREASE", label: "Mới nhất" },
  { value: "DATE_INCREASE", label: "Cũ nhất" },
];
const statusOptions = [
  { value: "all", label: "Tất cả" },
  { value: false, label: "Không hoạt động" },
  { value: true, label: "Hoạt động" },
];
const typeOptions = [
  { value: "all", label: "Tất cả" },
  { value: true, label: "Đặc biệt" },
  { value: false, label: "Thường" },
];
const customStyles = {
  control: (provided) => ({
    ...provided,
    minHeight: "34px",
    borderRadius: "20px", // Set the height here
  }),
  valueContainer: (provided) => ({
    ...provided,
    height: "34px",
    padding: "0 6px",
  }),
  input: (provided) => ({
    ...provided,
    margin: "0px",
  }),
  indicatorSeparator: (provided) => ({
    display: "none", // Hide the separator
  }),
  indicatorsContainer: (provided) => ({
    ...provided,
    height: "34px",
  }),
};

const Products = () => {
  const [totalPage, setTotalPage] = useState(1);
  const [products, setProducts] = useState([]);
  const handleGetAllProduct = async (search) => {
    try {
      const data = await ProductService.getAll(search || "");
      setProducts(data?.datas);
      setTotalPage(data?.totalPage);
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };

  const searchRef = useRef();

  const { search } = useLocation();
  const getQueryParam = (name) => {
    const searchParams = new URLSearchParams(search);
    return searchParams.get(name) || (name == "page" ? "1" : "");
  };

  const [selectedOption, setSelectedOption] = useState(options[0]);

  const handleChange = (option) => {
    setSelectedOption(option);
  };

  const [selectedTypeOption, setSelectedTypeOption] = useState(typeOptions[0]);

  const handleTypeChange = (option) => {
    setSelectedTypeOption(option);
  };

  const [selectedstatusOption, setSelectedstatusOption] = useState(
    statusOptions[0]
  );

  const handlestatusChange = (option) => {
    setSelectedstatusOption(option);
  };

  const handleSearch = () => {
    const filteredSearch = {
      search: searchRef.current.value,
      sort: selectedOption?.value,
      page: getQueryParam("page"),
      size: 10,
      isSpecial: selectedTypeOption?.value,
      status: selectedstatusOption?.value,
    };
    if (filteredSearch.status == "all") {
      delete filteredSearch.status;
    }
    if (filteredSearch.isSpecial == "all") {
      delete filteredSearch.isSpecial;
    }
    const excludesFields = ["search", "sort", "role"];
    excludesFields?.forEach((item) => {
      if (!filteredSearch[item]) {
        delete filteredSearch[item];
      }
    });
    const searchURL = new URLSearchParams(filteredSearch);
    handleGetAllProduct(`${searchURL}`);
  };
  useEffect(() => {
    handleSearch();
  }, [search]);
  return (
    <div>
      <div className="search_form">
        <div className="search_form_wrap">
          <div className="search_form_item">
            <input
              ref={searchRef}
              className="search_form_input"
              placeholder="Tìm kiếm..."
            />
          </div>
          <div className="search_form_item">
            <label style={{ marginRight: "10px" }}>Sắp xếp: </label>
            <Select
              styles={customStyles}
              value={selectedOption}
              onChange={handleChange}
              options={options}
            />
          </div>
          <div className="search_form_item">
            <label style={{ marginRight: "10px" }}>Thể loại: </label>
            <Select
              styles={customStyles}
              value={selectedTypeOption}
              onChange={handleTypeChange}
              options={typeOptions}
            />
          </div>
          <div className="search_form_item">
            <label style={{ marginRight: "10px" }}>Trạng thái: </label>
            <Select
              styles={customStyles}
              value={selectedstatusOption}
              onChange={handlestatusChange}
              options={statusOptions}
            />
          </div>
          <div>
            <div>
              <button
                onClick={handleSearch}
                className="btn btn-primary"
                style={{
                  borderRadius: "40px",
                  height: "34px",
                  fontSize: "13px",
                }}
              >
                Tìm kiếm
              </button>
            </div>
          </div>
        </div>
      </div>
      <section style={{ marginTop: "15px" }} className="ftco-section">
        <div className="row">
          <div className="col-md-12">
            <div className="table-wrap">
              <table className="table table-responsive-xl">
                <thead>
                  <tr>
                    <th style={{ width: "10%" }}>ID</th>
                    <th style={{ width: "25%" }}>Tên</th>
                    <th style={{ width: "20%" }}>Loại sản phẩm</th>
                    <th style={{ width: "25%" }}>Người Bán</th>
                    <th style={{ width: "20%" }}>Trạng thái</th>
                  </tr>
                </thead>
                <tbody>
                  {products?.map((item, index) => (
                    <ProductCard
                      item={item}
                      key={item?.productID}
                      index={index}
                    />
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>
      <Pagination count={totalPage} />
    </div>
  );
};

export default Products;
