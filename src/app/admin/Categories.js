import React, {
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import { useDropzone } from "react-dropzone";
import "./customStyle.scss";
import CategoryService from "../service/CategoryService";
import ImageService from "../service/ImageService";
import { toast } from "react-toastify";
import Pagination from "../component/pagination/Pagination";
import { useLocation } from "react-router-dom";
import { UserContext } from "../../App";
import Select from "react-select";

const options = [
  { value: "DATE_DECREASE", label: "Mới nhất" },
  { value: "DATE_INCREASE", label: "Cũ nhất" },
];
const statusOptions = [
  { value: "all", label: "Tất cả" },
  { value: false, label: "Không hoạt động" },
  { value: true, label: "Hoạt động" },
];
const customStyles = {
  control: (provided) => ({
    ...provided,
    minHeight: "34px",
    borderRadius: "20px", // Set the height here
  }),
  valueContainer: (provided) => ({
    ...provided,
    height: "34px",
    padding: "0 6px",
  }),
  input: (provided) => ({
    ...provided,
    margin: "0px",
  }),
  indicatorSeparator: (provided) => ({
    display: "none", // Hide the separator
  }),
  indicatorsContainer: (provided) => ({
    ...provided,
    height: "34px",
  }),
};
const Categories = () => {
  const [addNewShow, setAddNewShow] = useState(false);
  const [categories, setCategories] = useState([]);
  const [totalPage, setTotalPage] = useState(1);
  const [reload, setReload] = useState(false);

  const searchRef = useRef();

  const [current, setCurrent] = useState(null);

  const { search } = useLocation();
  const getQueryParam = (name) => {
    const searchParams = new URLSearchParams(search);
    return searchParams.get(name) || (name == "page" ? "1" : "");
  };

  const handleGetAllCategory = async (search) => {
    try {
      const data = await CategoryService.getAll(search);
      setCategories(data?.data?.datas);
      setTotalPage(data?.data?.totalPage);
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };

  const handleChangeStatusCategory = async (id) => {
    try {
      await CategoryService.changeStatus(id);
      toast.success("Cập nhật status thành công!");
      setReload((pre) => !pre);
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };
  const handleDeleteItem = async (item) => {
    try {
      const confirm = window.confirm(
        `Bạn có thực sự muốn xóa ${item?.categoryName} không?`
      );
      if (confirm) {
        const data = await CategoryService.delete(item?.categoryID);
        toast.success(data?.data);
        setReload((pre) => !pre);
      }
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };

  useEffect(() => {
    handleSearch();
  }, [reload, search]);

  const [selectedOption, setSelectedOption] = useState(options[0]);

  const handleChange = (option) => {
    setSelectedOption(option);
  };

  const [selectedstatusOption, setSelectedstatusOption] = useState(
    statusOptions[0]
  );

  const handlestatusChange = (option) => {
    setSelectedstatusOption(option);
  };

  const handleSearch = () => {
    const filteredSearch = {
      search: searchRef.current.value,
      sort: selectedOption?.value,
      page: getQueryParam("page"),
      size: 10,
      status: selectedstatusOption?.value,
    };
    if (filteredSearch.status == "all") {
      delete filteredSearch.status;
    }
    const excludesFields = ["search", "sort", "role"];
    excludesFields?.forEach((item) => {
      if (!filteredSearch[item]) {
        delete filteredSearch[item];
      }
    });
    const searchURL = new URLSearchParams(filteredSearch);
    handleGetAllCategory(searchURL.toString());
  };

  return (
    <div>
      <div
        style={{
          display: "flex",
          marginTop: "20px",
          justifyContent: "flex-end",
        }}
      >
        <button
          onClick={() => {
            setCurrent(null);
            setAddNewShow(true);
          }}
          className="btn btn-primary"
        >
          Thêm mới
        </button>
      </div>
      <div className="search_form">
        <div className="search_form_wrap">
          <div className="search_form_item">
            <input
              ref={searchRef}
              className="search_form_input"
              placeholder="Tìm kiếm..."
            />
          </div>
          <div className="search_form_item">
            <label style={{ marginRight: "10px" }}>Sắp xếp: </label>
            <Select
              styles={customStyles}
              value={selectedOption}
              onChange={handleChange}
              options={options}
            />
          </div>
          <div className="search_form_item">
            <label style={{ marginRight: "10px" }}>Trạng thái: </label>
            <Select
              styles={customStyles}
              value={selectedstatusOption}
              onChange={handlestatusChange}
              options={statusOptions}
            />
          </div>
          <div>
            <div>
              <button
                onClick={handleSearch}
                className="btn btn-primary"
                style={{
                  borderRadius: "40px",
                  height: "34px",
                  fontSize: "13px",
                }}
              >
                Tìm kiếm
              </button>
            </div>
          </div>
        </div>
      </div>
      {(!categories || categories?.length == 0) && (
        <div style={{ textAlign: "center", width: "100%" }}>
          <p style={{ fontSize: "16px", color: "red" }}>
            (Không tìm thấy dữ liệu)
          </p>
        </div>
      )}
      <section style={{ marginTop: "15px" }} className="ftco-section">
        <div className="row">
          <div className="col-md-12">
            <div className="table-wrap">
              <table className="table table-responsive-xl">
                <thead>
                  <tr>
                    <th style={{ width: "10%" }}>ID</th>
                    <th style={{ width: "20%" }}>Icon</th>
                    <th style={{ width: "25%" }}>Tên</th>
                    <th style={{ width: "20%" }}>Trạng thái</th>
                    <th style={{ width: "25%" }}>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  {categories?.map((item) => (
                    <tr
                      key={item?.categoryID + "A"}
                      className="alert"
                      role="alert"
                    >
                      <td className="border-bottom-0-custom">
                        {item?.categoryID}
                      </td>
                      <td className="d-flex align-items-center border-bottom-0-custom">
                        <div className="img">
                          <img
                            style={{
                              width: "45px",
                              height: "45px",
                              borderRadius: "50%",
                              objectFit: "cover",
                            }}
                            src={item?.categoryImage}
                          />
                        </div>
                      </td>
                      <td className="border-bottom-0-custom">
                        {item?.categoryName}
                      </td>
                      <td className="status border-bottom-0-custom">
                        <span
                          className={item?.status ? "active" : "inactiveColor"}
                        >
                          {item?.status ? "Hoạt động" : "KHông hoạt động"}
                        </span>
                      </td>
                      <td className="border-bottom-0-custom">
                        <button
                          onClick={() => {
                            handleChangeStatusCategory(item?.categoryID);
                          }}
                          style={{ height: "30px", fontSize: "12px" }}
                          type="button"
                          className={`btn ${
                            item?.status ? "btn-danger" : "btn-primary"
                          }`}
                        >
                          {!item?.status ? "Hoạt động" : "Không hoạt động"}
                        </button>
                        <button
                          onClick={() => {
                            setCurrent(item);
                            setAddNewShow(true);
                          }}
                          style={{
                            marginLeft: "5px",
                            height: "30px",
                            fontSize: "12px",
                          }}
                          type="button"
                          className="btn btn-success"
                        >
                          Sửa
                        </button>
                        <button
                          onClick={() => {
                            handleDeleteItem(item);
                          }}
                          style={{
                            marginLeft: "5px",
                            height: "30px",
                            fontSize: "12px",
                          }}
                          type="button"
                          className="btn btn-secondary"
                        >
                          Xóa
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>
      <Pagination count={totalPage} />
      {addNewShow && (
        <AddNewCategory
          setShow={setAddNewShow}
          setReload={setReload}
          current={current}
        />
      )}
    </div>
  );
};

const AddNewCategory = ({ setShow, current, setReload }) => {
  const [imageSrc, setImageSrc] = useState(current?.categoryImage || null);
  const [imageFile, setImageFile] = useState(null);

  const [error, setError] = useState({
    categoryName: "",
    categoryImage: "",
  });

  const { setLoading } = useContext(UserContext);

  const nameRef = useRef();

  const onDrop = useCallback((acceptedFiles) => {
    const file = acceptedFiles[0];

    if (file) {
      setImageFile(file);
      const img = new Image();
      const reader = new FileReader();

      reader.onload = (e) => {
        img.src = e.target.result;
      };

      img.onload = () => {
        setImageSrc(img.src);
      };

      reader.readAsDataURL(file);
    }
  }, []);

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: "image/*",
    maxFiles: 1,
  });

  const handleCreateCategory = async () => {
    try {
      let category = {
        categoryImage: current?.categoryImage || "",
        categoryName: nameRef.current.value,
      };

      let newErr = {};
      let isErr = false;

      if (!category.categoryName) {
        isErr = true;
        newErr["categoryName"] = "Tên thể loại không được bỏ trống!";
      }
      if (!imageSrc) {
        isErr = true;
        newErr["categoryImage"] = "Ảnh thể loại không được bỏ trống!";
      }
      setError({ ...newErr });
      if (!isErr) {
        setLoading(true);
        if (imageFile) {
          const url = await ImageService.uploadImagesToServer3S(
            imageFile,
            process.env.REACT_APP_CATE_FILE
          );
          category.categoryImage = url;
        }
        if (!current) {
          await CategoryService.create(category);
          toast.success("Tạo thành công!");
        } else {
          await CategoryService.update(current?.categoryID, category);
          toast.success("Cập nhật thành công!");
        }
        setShow(false);
        setReload((pre) => !pre);
        setLoading(false);
      }
    } catch (err) {
      setLoading(false);
      toast.error(err?.response?.data?.message);
    }
  };

  return (
    <div className="create_container">
      <div className="create_wrap">
        <div
          onClick={() => {
            setShow(false);
          }}
          className="create_close_wrap"
        >
          <i className="fa-solid fa-x"></i>
        </div>
        <div>
          {error?.categoryImage && (
            <div className="create_input_container">
              <i
                style={{
                  color: "red",
                  marginBottom: "-20px",
                  marginTop: "0px",
                }}
              >
                {error?.categoryImage}
              </i>
            </div>
          )}
          <div {...getRootProps()} className="dropzone_container">
            <input {...getInputProps()} />
            {imageSrc ? (
              <img src={imageSrc} alt="Uploaded" className="dropzone_image" />
            ) : (
              <p>Thêm ảnh vào đây</p>
            )}
          </div>
          {error?.categoryName && (
            <div className="create_input_container">
              <i
                style={{
                  color: "red",
                  marginBottom: "10px",
                  marginTop: "-10px",
                }}
              >
                {error?.categoryName}
              </i>
            </div>
          )}
          <div className="create_input_container">
            <input
              placeholder="Thể loại mới *"
              className="create_input"
              name="category"
              ref={nameRef}
              defaultValue={current?.categoryName}
            />
          </div>
          <div className="btn_create_container">
            <button
              onClick={handleCreateCategory}
              style={{ margin: "0 5px" }}
              className="btn btn-primary"
            >
              {current ? "Cập nhật" : "Tạo mới"}
            </button>
            <button
              onClick={() => {
                setShow(false);
              }}
              style={{ margin: "0 5px" }}
              className="btn btn-secondary"
            >
              Hủy
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Categories;
