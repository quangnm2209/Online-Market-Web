import React, { useCallback, useEffect, useRef, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination, Scrollbar, A11y } from "swiper/modules";
import { toast } from "react-toastify";
import { useDropzone } from "react-dropzone";
import ContactService from "../service/ContactService";
import MarketingService from "../service/MarketingService";
import ImageService from "../service/ImageService";
import Loading from "../component/loading/Loading";

const Common = () => {
  const [contact, setContact] = useState([]);
  const [marketingCards, setMarketingCards] = useState([]);
  const [qrCode, setQrCode] = useState("");
  const [loading, setLoading] = useState(false);

  const handleGet = async () => {
    try {
      const APIList = [
        handleGetContact(),
        handleGetMarketingCard(),
        handleGetQRCode(),
      ];
      await Promise.allSettled(APIList);
    } catch (err) {
      toast.error(err?.message);
    }
  };

  useEffect(() => {
    handleGet();
  }, []);

  const handleGetContact = async () => {
    try {
      const data = await ContactService.getContactList();
      setContact(data);
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };
  const handleGetMarketingCard = async () => {
    try {
      const data = await MarketingService.getMarketingCartList();
      if (data) {
        let values = JSON.parse(data.value);
        setMarketingCards(values);
      }
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };
  const handleGetQRCode = async () => {
    try {
      const data = await MarketingService.getqrcodeCartList();
      if (data && data?.value) {
        let value = JSON.parse(data?.value);
        setQrCode(value[0]);
      }
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };
  const handleDeleteContact = async (item) => {
    try {
      const check = window.confirm(
        `Bạn có thực sự muốn xóa ${item?.name} không?`
      );

      if (check) {
        const data = await ContactService.deleteContact(item?._id);
        toast.success(data?.message);
        handleGetContact();
      }
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };

  const hanldeAddMarketingCard = async (imageFile, index = -1) => {
    try {
      setLoading(true);
      let url = await ImageService.uploadImagesToServer3S(
        imageFile,
        process.env.REACT_APP_MARKETING_CART_FILE
      );
      const data = await MarketingService.addMarketingCard({
        value: url,
        position: index,
      });
      toast.success(data?.message);
      await handleGetMarketingCard();
      setLoading(false);
    } catch (err) {
      setLoading(false);
      toast.error(err?.response?.data?.message);
    }
  };
  const handleDeleteMarketingCard = async (index) => {
    try {
      setLoading(true);
      const data = await MarketingService.removeMarketingCard({
        position: index,
      });
      toast.success(data?.message);
      await handleGetMarketingCard();
      setLoading(false);
    } catch (err) {
      setLoading(false);
      toast.error(err?.response?.data?.message);
    }
  };
  const handleCUQRCode = async (imageFile) => {
    try {
      setLoading(true);
      let url = await ImageService.uploadImagesToServer3S(
        imageFile,
        process.env.REACT_APP_SYSTEM_QR_CODE_FILE
      );
      let storeURL = JSON.stringify([url]);
      const data = await MarketingService.CUqrcode({
        value: storeURL,
      });
      toast.success(data?.message);
      await handleGetQRCode();
      setLoading(false);
    } catch (err) {
      setLoading(false);
      toast.error(err?.response?.data?.message);
    }
  };
  return (
    <div>
      <div className="common_form">
        <Marketing
          cards={marketingCards}
          setReload={handleGetMarketingCard}
          onSuccess={hanldeAddMarketingCard}
          onDelete={handleDeleteMarketingCard}
        />
        <QRPayment
          onSuccess={handleCUQRCode}
          setReload={handleGetQRCode}
          qrCode={qrCode}
        />
      </div>
      <ContactList
        contact={contact}
        setReload={handleGetContact}
        handleDeleteContact={handleDeleteContact}
      />
      {loading && <Loading />}
    </div>
  );
};
const ContactList = ({
  contact,
  setReload = () => {},
  handleDeleteContact = () => {},
}) => {
  const [show, setShow] = useState(false);
  const [current, setCurrent] = useState(null);

  return (
    <div>
      <div
        style={{
          display: "flex",
          marginTop: "20px",
          justifyContent: "center",
        }}
      >
        <button
          onClick={() => {
            setCurrent(null);
            setShow(true);
          }}
          className="btn btn-primary"
        >
          Thêm mới liên hệ
        </button>
      </div>
      <section style={{ marginTop: "15px" }} className="ftco-section">
        <div className="row">
          <div className="col-md-12">
            <div className="table-wrap">
              <table className="table table-responsive-xl">
                <thead>
                  <tr>
                    <th style={{ width: "10%" }}>ID</th>
                    <th style={{ width: "20%" }}>Tên</th>
                    <th style={{ width: "30%" }}>Giá trị</th>
                    <th style={{ width: "10%" }}>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  {contact?.map((item, index) => (
                    <tr key={item?._id} className="alert" role="alert">
                      <td className="border-bottom-0-custom">{index + 1}</td>
                      <td className="border-bottom-0-custom">{item?.name}</td>
                      <td className="border-bottom-0-custom">
                        {item?.value?.includes("http") ? (
                          <a href={item?.value} target="_blank">
                            {item?.value}
                          </a>
                        ) : (
                          <div>{item?.value}</div>
                        )}
                      </td>
                      <td className="border-bottom-0-custom">
                        <button
                          onClick={() => {
                            setCurrent(item);
                            setShow(true);
                          }}
                          style={{
                            height: "30px",
                            fontSize: "12px",
                            marginLeft: "5px",
                          }}
                          type="button"
                          className="btn btn-primary"
                        >
                          Sửa
                        </button>
                        <button
                          onClick={() => {
                            handleDeleteContact(item);
                          }}
                          style={{
                            marginLeft: "5px",
                            height: "30px",
                            fontSize: "12px",
                          }}
                          type="button"
                          className="btn btn-danger"
                        >
                          Xóa
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>
      {show && (
        <AddNewContact
          setReload={setReload}
          setShow={setShow}
          current={current}
        />
      )}
    </div>
  );
};
const AddNewContact = ({ setShow, setReload, current }) => {
  const [err, setErr] = useState({
    planName: "",
    description: "",
  });

  const planNameRef = useRef();
  const descriptionRef = useRef();

  useEffect(() => {
    if (current) {
      planNameRef.current.value = current.name;
      descriptionRef.current.value = current.value;
    }
  }, [current]);

  const handleCreateNewRegistration = async () => {
    try {
      const registration = {
        planName: planNameRef.current.value,
        description: descriptionRef.current.value,
      };
      const registrationFeildName = {
        planName: "Tên",
        description: "Giá trị",
      };
      const excludesFields = [];
      let isErr = false;
      let newError = {};
      for (const [key, value] of Object.entries(registration)) {
        if (!excludesFields.includes(key)) {
          if (!registration[key]) {
            isErr = true;
            newError[key] =
              registrationFeildName[key] + " không được bỏ trống!";
          }
        }
      }
      setErr(newError);

      if (!isErr) {
        let data = {};
        if (current?._id) {
          data = await ContactService.updateContact(current?._id, {
            name: registration?.planName,
            value: registration?.description,
          });
        } else {
          data = await ContactService.createContact({
            name: registration?.planName,
            value: registration?.description,
          });
        }
        toast.success(data?.message);
        setReload();
        setShow(false);
      }
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };

  return (
    <div className="create_container">
      <div className="create_wrap">
        <div
          onClick={() => {
            setShow(false);
          }}
          className="create_close_wrap"
        >
          <i className="fa-solid fa-x"></i>
        </div>
        <div style={{ marginBottom: "20px", marginTop: "30px" }}>
          {err?.planName && (
            <div className="create_input_container">
              <i
                style={{
                  color: "red",
                  marginBottom: "10px",
                  marginTop: "-10px",
                }}
              >
                {err?.planName}
              </i>
            </div>
          )}
          <div className="create_input_container">
            <input
              ref={planNameRef}
              placeholder="Tên *"
              className="create_input"
              name="text"
              defaultValue={current?.planName}
            />
          </div>
          {err?.description && (
            <div className="create_input_container">
              <i
                style={{
                  color: "red",
                  marginBottom: "-10px",
                  marginTop: "10px",
                }}
              >
                {err?.description}
              </i>
            </div>
          )}
          <div style={{ marginTop: "20px" }} className="create_input_container">
            <textarea
              ref={descriptionRef}
              placeholder="Giá trị *"
              className="create_input"
              name="text"
              style={{ resize: "vertical" }}
              defaultValue={current?.planDescription}
            />
          </div>
          <div className="btn_create_container">
            <button
              onClick={handleCreateNewRegistration}
              style={{ margin: "0 5px" }}
              className="btn btn-primary"
            >
              {current?._id ? "Cập nhật" : "Tạo mới"}
            </button>
            <button
              onClick={() => {
                setShow(false);
              }}
              style={{ margin: "0 5px" }}
              className="btn btn-secondary"
            >
              Hủy
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

const AddNewImage = ({
  setShow,
  current,
  width = "300px",
  height = "150px",
  useAuto = false,
  onSuccess = (imgFile) => {},
}) => {
  const [imageSrc, setImageSrc] = useState(current?.categoryImage || null);
  const [imageFile, setImageFile] = useState(null);
  const [err, setErr] = useState({
    planName: "",
  });
  useEffect(() => {
    setImageSrc(current?.url);
  }, [current]);

  const onDrop = useCallback((acceptedFiles) => {
    const file = acceptedFiles[0];

    if (file) {
      setImageFile(file);
      const img = new Image();
      const reader = new FileReader();

      reader.onload = (e) => {
        img.src = e.target.result;
      };

      img.onload = () => {
        setImageSrc(img.src);
      };

      reader.readAsDataURL(file);
    }
  }, []);

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: "image/*",
    maxFiles: 1,
  });

  const handleCreateNewRegistration = async () => {
    try {
      if (!imageFile) {
        setErr({
          planName: "Vui lòng chọn ảnh",
        });
        return;
      }
      setErr({ planName: "" });
      onSuccess(imageFile);
      setShow(false);
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };

  return (
    <div className="create_container">
      <div className="create_wrap">
        <div
          onClick={() => {
            setShow(false);
          }}
          className="create_close_wrap"
        >
          <i className="fa-solid fa-x"></i>
        </div>
        <div style={{ marginBottom: "20px", marginTop: "30px" }}>
          {err?.planName && (
            <div className="create_input_container">
              <i
                style={{
                  color: "red",
                  marginBottom: "10px",
                  marginTop: "-10px",
                }}
              >
                {err?.planName}
              </i>
            </div>
          )}
          <div
            {...getRootProps()}
            style={
              useAuto
                ? {
                    width: "40%",
                    height: "auto",
                    minHeight: "200px",
                  }
                : {
                    width: width,
                    height: height,
                  }
            }
            className="dropzone_container"
          >
            <input {...getInputProps()} />
            {imageSrc ? (
              <img
                src={imageSrc}
                style={
                  useAuto
                    ? {
                        width: "100%",
                        height: "auto",
                      }
                    : {
                        width: "100%",
                        height: "100%",
                      }
                }
                alt="Uploaded"
                className="dropzone_image"
              />
            ) : (
              <p>Thêm ảnh vào đây</p>
            )}
          </div>
          <div className="btn_create_container">
            <button
              onClick={handleCreateNewRegistration}
              style={{ margin: "0 5px" }}
              className="btn btn-primary"
            >
              {current?.planID ? "Cập nhật" : "Tạo mới"}
            </button>
            <button
              onClick={() => {
                setShow(false);
              }}
              style={{ margin: "0 5px" }}
              className="btn btn-secondary"
            >
              Hủy
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
const QRPayment = ({ onSuccess, setReload = () => {}, qrCode = "" }) => {
  const [show, setShow] = useState(false);
  const [current, setCurrent] = useState(null);
  return (
    <div className="qrCode_container">
      <div className="qr_image">
        <div>
          <img src={qrCode} alt="Ảnh QR thanh toán" />
        </div>
      </div>
      <div style={{ textAlign: "center", marginTop: "10px" }}>
        <button
          onClick={() => {
            setCurrent({
              index: 0,
              url: qrCode,
            });
            setShow(true);
          }}
          className="btn btn-primary"
        >
          Thay đổi
        </button>
      </div>
      {show && (
        <AddNewImage
          useAuto={true}
          setReload={setReload}
          setShow={setShow}
          current={current}
          onSuccess={onSuccess}
        />
      )}
    </div>
  );
};
const Marketing = ({
  cards = [],
  setReload = () => {},
  onSuccess = () => {},
  onDelete = () => {},
}) => {
  const [show, setShow] = useState(false);
  const [current, setCurrent] = useState(null);
  const handleEdit = (index) => {
    setCurrent({
      index,
      url: cards[index],
    });
    setShow(true);
  };
  const handleDelete = (index) => {
    onDelete(index);
  };
  return (
    <div className="marketing">
      <div>
        <MarketingSwiper
          cards={cards}
          handleEdit={handleEdit}
          handleDelete={handleDelete}
        />
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginTop: "15px",
          }}
        >
          <button
            onClick={() => {
              setCurrent(null);
              setShow(true);
            }}
            className="btn btn-primary"
          >
            Thêm mới
          </button>
        </div>
      </div>
      {show && (
        <AddNewImage
          setReload={setReload}
          setShow={setShow}
          current={current}
          onSuccess={(file) => {
            onSuccess(file, current ? current?.index : -1);
          }}
        />
      )}
    </div>
  );
};
const MarketingSwiper = ({
  cards,
  handleEdit = () => {},
  handleDelete = () => {},
}) => {
  return (
    <Swiper
      spaceBetween={0}
      slidesPerView={3}
      modules={[Navigation, Pagination, Scrollbar, A11y]}
      navigation
      scrollbar={{ draggable: true }}
    >
      {cards?.map((item, index) => (
        <SwiperSlide key={item}>
          <MarketingCard
            url={item}
            handleEdit={() => {
              handleEdit(index);
            }}
            handleDelete={() => {
              handleDelete(index);
            }}
          />
        </SwiperSlide>
      ))}
    </Swiper>
  );
};
const MarketingCard = ({
  url,
  handleEdit = () => {},
  handleDelete = () => {},
}) => {
  const [bar, setbar] = useState(false);
  return (
    <div className="marketing_card">
      <img src={url} />
      <div
        onClick={() => {
          setbar((pre) => !pre);
        }}
        className="marketing_dots"
      >
        <i className="fa-solid fa-ellipsis"></i>
      </div>
      {bar && (
        <div className="marketing_btn">
          <div
            onClick={() => {
              handleEdit();
            }}
            className="marketing_btn_item"
          >
            Sửa
          </div>
          <div
            onClick={() => {
              handleDelete();
            }}
            className="marketing_btn_item"
          >
            Xóa
          </div>
        </div>
      )}
    </div>
  );
};

export default Common;
