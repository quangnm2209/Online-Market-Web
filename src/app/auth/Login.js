import React, { useContext, useRef, useState } from "react";
import "./style.scss";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import AuthAPI from "../service/AuthService";
import { UserContext } from "../../App";
const Login = () => {
  const emailRef = useRef();
  const passwordRef = useRef();

  const [msg, setMsg] = useState({});
  const { setReload, setLoading } = useContext(UserContext);

  const navigate = useNavigate();

  const handleLogin = async () => {
    const user = {
      email: emailRef.current?.value,
      password: passwordRef.current?.value,
    };
    let msgr = {};
    if (!user?.email) {
      msgr["gmail"] = "Email không được bỏ trống!";
    }
    if (!user.password) {
      msgr["password"] = "Mật khẩu không được bỏ trống!";
    }

    if (user.password.length < 8 && !msgr["password"]) {
      msgr["password"] = "Mật khẩu phải nhiều hơn 8 kí tự!";
    }
    if (msgr["gmail"] || msgr["password"]) {
      setMsg({ ...msgr });
      return;
    }
    try {
      setLoading(true);
      const data = await AuthAPI.login({ user });
      localStorage.setItem("token", data?.data?.accessToken);
      localStorage.setItem(
        "user",
        JSON.stringify({
          username: data?.data?.username,
          avatar: data?.data?.avatar,
        })
      );
      setLoading(false);
      toast.success("Đăng nhập thành công.");
      setReload((pre) => !pre);
      navigate("/admin/dashboard");
    } catch (err) {
      setLoading(false);
      let ms = {
        gmail: "Email hoặc mật khẩu không chính xác!",
        password: "Email hoặc mật khẩu không chính xác!",
      };

      setMsg({ ...ms });
      toast.error(err?.response?.data?.message);
    }
  };

  // useEffect(() => {
  // 	window.google?.accounts?.id?.initialize({
  // 		client_id:
  // 			"348299817023-08tbro4o6guo2csu2lv16mai16m4a8ce.apps.googleusercontent.com",
  // 		callback: handleCallbackGoogle,
  // 	});
  // 	window.google?.accounts?.id?.renderButton(
  // 		document.getElementById("loginGoogle"),
  // 		{
  // 			type: "icon",
  // 			theme: "outline",
  // 			size: "large",
  // 		}
  // 	);
  // 	window.google?.accounts?.id?.prompt();
  // }, [window.google?.accounts]);

  const handleCallbackGoogle = async (response) => {
    const user = parseJwt(response.credential);
    // try {
    // 	const data = await axios.post("/api/auth/login", {
    // 		gmail: user.email,
    // 		password: user.sub,
    // 		type: "google",
    // 	});
    // 	toast.success(data?.data?.msg);
    // 	navigate("/");
    // } catch (err) {
    // 	toast.error(err?.response?.data?.msg);
    // }
  };
  function parseJwt(token) {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
    var jsonPayload = decodeURIComponent(
      atob(base64)
        .split("")
        .map(function (c) {
          return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
        })
        .join("")
    );

    return JSON.parse(jsonPayload);
  }
  return (
    <div className="auth">
      <section>
        <div className="leaves">
          <div className="set">
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
          </div>
        </div>
        <img src="img/slider-1.jpg" className="bg" />
        <img
          src="https://res.cloudinary.com/sttruyen/image/upload/v1719453179/kftjt59wpbrze906xlxe.gif"
          className="girl"
        />
        <img
          src="https://res.cloudinary.com/sttruyen/image/upload/v1719453124/wezwltak1pbcrxqwmgnv.gif"
          className="girl1"
        />
        <img
          src="https://res.cloudinary.com/sttruyen/image/upload/v1719453124/wezwltak1pbcrxqwmgnv.gif"
          className="bikerboy"
        />
        <div className="login">
          <h2>Đăng nhập</h2>
          <div className="inputBox">
            {msg["gmail"] && (
              <div style={{ color: "red", margin: "10px 0", fontSize: "14px" }}>
                * <i>{msg["gmail"]}</i>
              </div>
            )}
            <input type="text" ref={emailRef} placeholder="gmail" name="" />
          </div>
          {msg["password"] && (
            <div
              style={{
                color: "red",
                marginBottom: "-20px",
                fontSize: "14px",
              }}
            >
              * <i>{msg["password"]}</i>
            </div>
          )}
          <div className="inputBox">
            <input
              type="password"
              ref={passwordRef}
              placeholder="Mật khẩu"
              name=""
            />
            <div
              onClick={() => {
                if (passwordRef.current) {
                  if (passwordRef.current.type === "text") {
                    passwordRef.current.type = "password";
                  } else {
                    passwordRef.current.type = "text";
                  }
                }
              }}
              className="eyes_items"
            >
              <i className="fa-solid fa-eye"></i>
            </div>
          </div>
          <div className="inputBox">
            <input
              onClick={handleLogin}
              type="submit"
              value="Đăng nhập"
              id="btn"
            />
          </div>
          <div style={{ justifyContent: "flex-end" }} className="group">
            <Link to="/forgot_password">Quên mật khẩu</Link>
          </div>
          {/* <div className="auth_wrap_other">
            <div id="loginGoogle"></div>
          </div> */}
        </div>
      </section>
      {/* <HomeIcons /> */}
    </div>
  );
};

export default Login;
