import axios from "axios";
import React, { useEffect } from "react";

const Zalo = () => {
  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);
    const code = urlParams.get("code");
    if (code) {
      const redirectUrl = `onlinemarket://(auth)/login?code=${code}`;
      window.location.href = redirectUrl;
    }
  }, []);
  const handlePressBack = () => {
    const urlParams = new URLSearchParams(window.location.search);
    const code = urlParams.get("code");
    if (code) {
      const redirectUrl = `onlinemarket://(auth)/login?code=${code}`;
      window.location.href = redirectUrl;
    }
  };

  return (
    <div>
      <button onClick={handlePressBack} className="btn btn-primary">
        Quay lại ứng dụng
      </button>
    </div>
  );
};

export default Zalo;
