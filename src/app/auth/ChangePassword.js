import React, { useEffect, useRef, useState } from "react";
import "./style.scss";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import AuthService from "../service/AuthService";
const ChangePassword = () => {
  const rePasswordRef = useRef();
  const passwordRef = useRef();
  const oldPasswordRef = useRef();

  const [msg, setMsg] = useState({});

  const navigate = useNavigate();

  const handleChangePassword = async () => {
    const user = {
      password: passwordRef.current?.value,
      rePassword: rePasswordRef.current?.value,
      oldPassword: oldPasswordRef.current?.value,
    };
    let msgr = {};
    let err = false;
    const excludesFields = ["password", "rePassword", "oldPassword"];
    const fieldName = {
      password: "Mật khẩu",
      rePassword: "Nhập lại mật khẩu",
      oldPassword: "Mật khẩu cũ",
    };
    excludesFields.forEach((item) => {
      if (!user[item]) {
        msgr[item] = fieldName[item] + " không được trống!";
        err = true;
      } else {
        if (user[item]?.length < 8) {
          msgr[item] = fieldName[item] + " phải lớn hơn 8 kí tự!";
          err = true;
        }
      }
    });
    if (user.password !== user.rePassword) {
      msgr["password"] = "Mật khẩu không trùng nhau!";
      msgr["rePassword"] = "Mật khẩu không trùng nhau!";
      err = true;
    }
    if (err) {
      setMsg({ ...msgr });
      return;
    }
    try {
      const data = await AuthService.ChangePassword({
        oldPassword: user.oldPassword,
        newPassword: user.password,
      });
      toast.success(data?.data?.message);
      navigate(-1);
    } catch (err) {
      toast.error(err?.response?.data?.message);
      setMsg({
        ...err?.response?.data,
      });
    }
  };
  return (
    <div className="auth">
      <section>
        <div className="leaves">
          <div className="set">
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
          </div>
        </div>
        <img src="img/slider-1.jpg" className="bg" />
        <img
          src="https://res.cloudinary.com/sttruyen/image/upload/v1719453179/kftjt59wpbrze906xlxe.gif"
          className="girl"
        />
        <img
          src="https://res.cloudinary.com/sttruyen/image/upload/v1719453124/wezwltak1pbcrxqwmgnv.gif"
          className="girl1"
        />
        <img
          src="https://res.cloudinary.com/sttruyen/image/upload/v1719453124/wezwltak1pbcrxqwmgnv.gif"
          className="bikerboy"
        />
        <div className="login">
          <h2>Đổi mật khẩu</h2>
          {msg["oldPassword"] && (
            <div
              style={{
                color: "red",
                marginBottom: "-20px",
                fontSize: "14px",
              }}
            >
              * <i>{msg["oldPassword"]}</i>
            </div>
          )}
          <div className="inputBox">
            <input
              type="password"
              ref={oldPasswordRef}
              placeholder="Mật khẩu cũ"
              name=""
            />
            <div
              onClick={() => {
                if (oldPasswordRef.current) {
                  if (oldPasswordRef.current.type === "text") {
                    oldPasswordRef.current.type = "password";
                  } else {
                    oldPasswordRef.current.type = "text";
                  }
                }
              }}
              className="eyes_items"
            >
              <i
                style={{ fontSize: "20px", marginRight: "10px" }}
                className="fa fa-solid fa-eye"
              ></i>
            </div>
          </div>
          {msg["password"] && (
            <div
              style={{
                color: "red",
                marginBottom: "-20px",
                fontSize: "14px",
              }}
            >
              * <i>{msg["password"]}</i>
            </div>
          )}
          <div className="inputBox">
            <input
              type="password"
              ref={passwordRef}
              placeholder="Mật khẩu mới"
              name=""
            />
            <div
              onClick={() => {
                if (passwordRef.current) {
                  if (passwordRef.current.type === "text") {
                    passwordRef.current.type = "password";
                  } else {
                    passwordRef.current.type = "text";
                  }
                }
              }}
              className="eyes_items"
            >
              <i
                style={{ fontSize: "20px", marginRight: "10px" }}
                className="fa fa-solid fa-eye"
              ></i>
            </div>
          </div>
          {msg["rePassword"] && (
            <div
              style={{
                color: "red",
                marginBottom: "-20px",
                fontSize: "14px",
              }}
            >
              * <i>{msg["rePassword"]}</i>
            </div>
          )}
          <div className="inputBox">
            <input
              type="password"
              ref={rePasswordRef}
              placeholder="Nhập lại mật khẩu mới"
              name=""
            />
            <div
              onClick={() => {
                if (rePasswordRef.current) {
                  if (rePasswordRef.current.type === "text") {
                    rePasswordRef.current.type = "password";
                  } else {
                    rePasswordRef.current.type = "text";
                  }
                }
              }}
              className="eyes_items"
            >
              <i
                style={{ fontSize: "20px", marginRight: "10px" }}
                className="fa-solid fa-eye"
              ></i>
            </div>
          </div>
          <div className="inputBox">
            <input
              onClick={handleChangePassword}
              type="submit"
              value="Đổi mật khẩu"
              id="btn"
            />
          </div>
        </div>
      </section>
    </div>
  );
};

export default ChangePassword;
