import React, { useEffect, useState } from "react";
import BackBtn from "../component/btn/BackBtn";
import { Link, useNavigate, useParams } from "react-router-dom";
import AccountService from "../service/AccountService";
import { toast } from "react-toastify";
import "../admin/style.scss";
import "../admin/customStyle.scss";
import moment from "moment";
import ProductCard from "../component/card/ProductCard";
const UserProfile = () => {
  const [type, setType] = useState("");

  const { id } = useParams();

  const [user, setUser] = useState({});

  const navigate = useNavigate();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const handleGetProfile = async () => {
    try {
      const data = await AccountService.getProfile(id);
      setUser(data?.data);
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };
  useEffect(() => {
    handleGetProfile();
  }, [id]);

  return (
    <div className="profile">
      <section style={{ backgroundColor: "#E1E4EB", minHeight: "100vh" }}>
        <div className="container py-5">
          <div className="row">
            <div className="col-lg-4">
              <div className="card mb-4">
                <div className="card-body text-center">
                  <img
                    src={
                      user?.avatar ||
                      "https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp"
                    }
                    alt="avatar"
                    className="rounded-circle img-fluid"
                    style={{
                      width: "150px",
                      border: "1px solid rgba(0,0,0,0.1)",
                    }}
                  />
                </div>
              </div>
              <div className="card mb-4 mb-lg-0">
                <div className="card-body p-0">
                  <ul className="list-group list-group-flush rounded-3">
                    <li className="list-group-item p-3">
                      <h5 className="mb-2">About Me</h5>
                      <p className="mb-0">
                        {user?.zaloID
                          ? "Đăng ký bằng zalo lúc "
                          : "Đăng ký lúc "}
                        {user?.registryDate}
                      </p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-lg-8">
              <div className="card mb-4">
                <div className="card-body">
                  <div className="row">
                    <div className="col-sm-3">
                      <p className="mb-0">Tên người dùng</p>
                    </div>
                    <div className="col-sm-9">
                      <p className="text-muted mb-0">{user?.username}</p>
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-sm-3">
                      <p className="mb-0">Email</p>
                    </div>
                    <div className="col-sm-9">
                      <p className="text-muted mb-0">
                        {user?.email || "(Không có)"}
                      </p>
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-sm-3">
                      <p className="mb-0">Số điện thoại</p>
                    </div>
                    <div className="col-sm-9">
                      <p className="text-muted mb-0">
                        {user?.phoneNumber || "(Không có)"}
                      </p>
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-sm-3">
                      <p className="mb-0">Vai trò</p>
                    </div>
                    <div className="col-sm-9">
                      <p className="text-muted mb-0">{user?.roleName}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div style={{ padding: "10px" }} className="card mb-4">
                {user?.roleName == "STORE_OWNER" && type === "" && (
                  <ProductSection
                    products={user?.products}
                    username={user?.username}
                  />
                )}
                {user?.roleName != "STORE_OWNER" &&
                  "(Bạn không phải là tiểu thương)"}
                <div style={{ marginBottom: "20px" }}></div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <BackBtn />
    </div>
  );
};

const ProductSection = ({ products, username }) => {
  return (
    <section style={{ marginTop: "15px" }} className="ftco-section">
      <div className="row">
        <div className="col-md-12">
          <div className="table-wrap">
            <table className="table table-responsive-xl">
              <thead>
                <tr>
                  <th style={{ width: "10%" }}>ID</th>
                  <th style={{ width: "25%" }}>Tên</th>
                  <th style={{ width: "20%" }}>Loại sản phẩm</th>
                  <th style={{ width: "25%" }}>Người Bán</th>
                  <th style={{ width: "20%" }}>Trạng thái</th>
                </tr>
              </thead>
              <tbody>
                {products?.map((item, index) => (
                  <ProductCard
                    item={item}
                    key={item?.productID + "userprofile"}
                    index={index}
                    seller={username}
                    link={false}
                  />
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>
  );
};

export default UserProfile;
