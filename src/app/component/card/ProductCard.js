import { Link } from "react-router-dom";

const ProductCard = ({ item, index, seller = "", link = true }) => {
  return (
    <tr className="alert" role="alert">
      <td className="border-bottom-0-custom">{index + 1}</td>
      <td className="d-flex align-items-center border-bottom-0-custom">
        <div className="img">
          <img
            style={{
              width: "45px",
              height: "45px",
              borderRadius: "50%",
              objectFit: "cover",
            }}
            src={item?.productImage}
          />
        </div>
        <div className="pl-3 email">
          <span>
            <div>{item?.productName}</div>
          </span>
          <span>Loại: {item?.categoryName}</span>
        </div>
      </td>
      <td className="border-bottom-0-custom">
        <div>{item?.special ? "Đặc biệt" : "Thường"}</div>
      </td>
      <td className="border-bottom-0-custom">
        {link ? (
          <Link to={`/user/profile/${item?.accountID}`}>
            {seller || item?.accountName}
          </Link>
        ) : (
          <div>
            {seller || item?.accountName}
          </div>
        )}
      </td>
      <td className="status border-bottom-0-custom">
        <span className={item?.status ? "active" : "inactiveColor"}>
          {item?.status ? "Hoạt động" : "Không hoạt động"}
        </span>
      </td>
    </tr>
  );
};

export default ProductCard;
