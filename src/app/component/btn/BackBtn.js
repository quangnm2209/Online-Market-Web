import React from "react";
import { useNavigate } from "react-router-dom";

const BackBtn = () => {
  const navigate = useNavigate();

  return (
    <div className="back-btn">
      <div
        onClick={() => {
          navigate(-1);
        }}
        className="fancy"
      >
        <span className="top-key"></span>
        <span className="text">Quay lại</span>
        <span className="bottom-key-1"></span>
        <span className="bottom-key-2"></span>
      </div>
    </div>
  );
};

export default BackBtn;
