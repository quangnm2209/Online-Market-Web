import axiosInstance from "../component/axios_custom/NodeJsAxiosCustom";
const MarketingService = {
  getMarketingCartList: async () => {
    try {
      const resposne = await axiosInstance.get("/common/marketing");
      return resposne.data;
    } catch (err) {
      throw err;
    }
  },
  addMarketingCard: async (data) => {
    try {
      const response = await axiosInstance.post(
        "/common/add/value/marketing",
        data
      );
      return response.data;
    } catch (err) {
      throw err;
    }
  },
  removeMarketingCard: async (data) => {
    try {
      const response = await axiosInstance.post(
        "/common/remove/position/marketing",
        data
      );
      return response.data;
    } catch (err) {
      throw err;
    }
  },
  getqrcodeCartList: async () => {
    try {
      const resposne = await axiosInstance.get("/common/qrcode");
      return resposne.data;
    } catch (err) {
      throw err;
    }
  },
  CUqrcode: async (data) => {
    try {
      const response = await axiosInstance.post("/common/set/qrcode", data);
      return response.data;
    } catch (err) {
      throw err;
    }
  },
};
export default MarketingService;
