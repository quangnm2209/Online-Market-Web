import axiosInstance from "../component/axios_custom/AxiosCustom";
const BeingService = {
  getAll: async (search) => {
    if (!search) {
      search = "page=1&size=10";
    }
    return await axiosInstance.get(
      `/api/admin/registration-plans/store_owner_request?${search}`
    );
  },
  reviewRequest: async (data) => {
    return await axiosInstance.put(
      `/api/admin/registration-plans/store_owner_request/review`,
      data
    );
  },
};

export default BeingService;
