import AxiosCustom from "../component/axios_custom/AxiosCustom";

const RegistrationService = {
  create: async (registration) => {
    return await AxiosCustom.post(
      "/api/admin/registration-plans/create",
      registration
    );
  },
  getAll: async (search) => {
    if (!search) {
      search = "page=1&size=10";
    }
    return await AxiosCustom.get(`/api/common/plans?${search}`);
  },
  update: async (registration) => {
    return await AxiosCustom.put(
      "/api/admin/registration-plans/update",
      registration
    );
  },
  changeStatus: async (planID, status) => {
    return await AxiosCustom.put(
      "/api/admin/registration-plans/change_plan_status",
      {
        planID,
        status,
      }
    );
  },
  delete: async (id) => {
    return await AxiosCustom.delete(
      `/api/admin/registration-plans/delete?planID=${id}`
    );
  },
};

export default RegistrationService;
