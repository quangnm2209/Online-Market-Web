import axiosInstance from "../component/axios_custom/AxiosCustom";
const ProductService = {
  getAll: async (search) => {
    try {
      if (!search) {
        search = `page=1&size=10`;
      }
      const res = await axiosInstance.get(
        `/api/admin/dashboard/product/list?${search}`
      );
      return res.data;
    } catch (err) {
      throw err;
    }
  },
};

export default ProductService;
