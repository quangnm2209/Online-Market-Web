import axios from "axios";
import { toast } from "react-toastify";
import axiosInstance from "../component/axios_custom/AxiosCustom";
const cloudinaryAxios = axios.create({
  baseURL: "https://api.cloudinary.com/v1_1/sttruyen/image/upload",
});

const ImageService = {
  uploadImagesToServer3S: async (file, type = "") => {
    try {
      if (!type) {
        type = process.env.REACT_APP_PRODUCT_FILE;
      }
      const filename = file.name;
      const match = /\.(\w+)$/.exec(filename);
      const fileExtension = match ? match[1] : "";
      const allowedExtensions = ["jpg", "jpeg", "png", "gif", "bmp"];

      if (!allowedExtensions.includes(fileExtension.toLowerCase())) {
        throw new Error(
          `File không phải 1 trong các thể loại sau: ${allowedExtensions.join(
            ", "
          )}`
        );
      }
      const formData = new FormData();
      formData.append("file", file);
      formData.append("folder", type);

      const res = await axiosInstance.post(
        "/api/common/uploadImage",
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );

      return res?.data?.key;
    } catch (err) {
      toast.error(err?.message);
    }
  },
  updateImageURLToServer3S: async (imageUrl, type = "") => {
    try {
      const response = await fetch(imageUrl);

      if (!response.ok) {
        throw new Error(`Failed to fetch image: ${response.statusText}`);
      }

      const contentType = response.headers.get("content-type");
      if (!contentType || !contentType.startsWith("image/")) {
        throw new Error(
          `URL does not point to an image. Content-Type: ${contentType}`
        );
      }
      const blob = await response.blob();

      const fileExtension = contentType.split("/")[1];
      const file = new File([blob], `image.${fileExtension}`, { type: contentType });

      const uploadResult = await ImageService.uploadImagesToServer3S(
        file,
        type
      );

      return uploadResult;
    } catch (err) {
      console.error("Error in fetchImageAndUpload:", err);
      toast.error(err.message);
    }
  },
};

export default ImageService;
