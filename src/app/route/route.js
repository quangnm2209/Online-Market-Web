import Admin from "../admin/Admin";
import Login from "../auth/Login";
import ForgotPassword from "../auth/ForgotPassword";
import ChangePassword from "../auth/ChangePassword";
import Home from "../home/Home";
import Zalo from "../auth/Zalo";
import UserProfile from "../user/UserProfile";
export const publicRouter = [
  {
    element: Home,
    path: "/",
  },
  {
    element: Login,
    path: "/login",
  },
  {
    element: ChangePassword,
    path: "/change_password",
  },
  {
    element: ForgotPassword,
    path: "/forgot_password",
  },
  {
    element: Zalo,
    path: "/zalo",
  },
  {
    element: UserProfile,
    path: "/user/profile/:id",
  },
];
export const adminRouter = [
  {
    element: Admin,
    path: "/admin/:slug",
  },
];
